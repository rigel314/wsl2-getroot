package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// /proc/mounts should contain lines with space-separated data in the form:
// device mount-point filesystem options dump pass
// We only care about the first two.  If something is mounted on /, that's the device we want.

func main() {
	f := mustOpen("/proc/mounts")
	defer f.Close()

	brd := bufio.NewReader(f)
	for {
		line := mustReadString(brd, '\n')
		sections := strings.Split(line, " ")
		if len(sections) < 2 {
			continue
		}
		if sections[1] == "/" {
			fmt.Println(sections[0])
			return
		}
	}
}

func mustOpen(path string) *os.File {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	return f
}

func mustReadString(rd *bufio.Reader, delim byte) string {
	str, err := rd.ReadString(delim)
	if err != nil {
		panic(err)
	}

	return str
}
