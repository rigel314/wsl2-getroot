wsl2-getroot
======

## About
This repo creates a tar file you can import as a wsl2 distro.  Which can then be easily attached to a different wsl2 distro.  This comes in handy when you want do install a big thing, and maybe uninstall it later and you don't want your main .vhdx file to grow massively.

This works because wsl2 allows multiple distros, but they all run in one VM.  When you "start" one of the distros, wsl2 just connects the vhdx for that distro to the VM.  It seems to stay connected for the duration of the VM, even if the same distro becomes "stopped".  Once you have the vhdx connected to the VM, it's trivial to mount /dev/sdb or whatever into a regular folder.

## Usage
* grab the latest getroot.tar from [the CI](https://gitlab.com/rigel314/wsl2-getroot/-/jobs/artifacts/master/raw/getroot.tar?job=buildjob)
* Import getroot.tar a few times for each data drive you need
  * `wsl.exe --import data1 C:/wsl/data1 getroot.tar --version 2`
  * `wsl.exe --import data2 C:/wsl/data2 getroot.tar --version 2`
* mount the data drives in WSL2 by running something like the example code in [`attach.sh`](https://gitlab.com/rigel314/wsl2-getroot/-/blob/master/attach.sh)
