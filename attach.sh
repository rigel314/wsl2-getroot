#!/bin/bash

# This is an example of how you would attach a "data" wsl2 distro from a regular wsl2 distro.  This assumes you used the same distro names as in the README.

dev=$(/mnt/c/Windows/System32/wsl.exe -d data1 /getroot)
sudo mkdir -p /mnt/data/1
sudo mount $dev /mnt/data/1

dev=$(/mnt/c/Windows/System32/wsl.exe -d data2 /getroot)
sudo mkdir -p /mnt/data/2
sudo mount $dev /mnt/data/2
